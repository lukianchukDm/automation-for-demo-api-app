package net.portnov;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class CreateStudentTests {

    @Test
    public void getStudentsTest() {
        JSONObject body = new JSONObject();
        body.put("fullname", "Chris Evans");
        body.put("registrationDate", "2020-12-31");

        JSONObject deposit = new JSONObject();
        deposit.put("isPaid", true);
        deposit.put("amount", 500);
        body.put("deposit", deposit);

        body.put("group", "Campus");

        String[] classes = {"manual", "automation", "api"};
        body.put("classes", classes);

        Response response = RestAssured.given()
                .header(new Header("x-access-token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"))
                .contentType(ContentType.JSON)
                .body(body.toString())
                .post("https://portnov-students-demo-api.herokuapp.com/students");

        response.print();
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 201, "Status code is wrong");

        String fullname = response.jsonPath().getString("fullname");
        String registrationDate = response.jsonPath().getString("registrationDate");
        boolean isPaid = response.jsonPath().getBoolean("deposit.isPaid");
        List<String> actualClasses = response.jsonPath().getList("classes");
        int id = response.jsonPath().getInt("id");

        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(fullname, "Chris Evans", "The fullname is wrong");
        softAssert.assertEquals(registrationDate, "2020-12-31", "The registrationDate is wrong");
        softAssert.assertTrue(isPaid, "isPaid is wrong");
        softAssert.assertEquals(actualClasses.get(0), classes[0], "First class is wrong");
        softAssert.assertEquals(id, 2, "ID is wrong");

        softAssert.assertAll();
    }
}
