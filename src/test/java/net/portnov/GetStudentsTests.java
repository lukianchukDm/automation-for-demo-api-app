package net.portnov;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class GetStudentsTests {

    @Test
    public void getStudentsTest() {
        Response response = RestAssured.get("https://portnov-students-demo-api.herokuapp.com/students");
        response.print();

        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200, "Status code is wrong");

        List<String> students = response.jsonPath().getList("");

        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(students.size(), 3, "The size of students' list is wrong");
        softAssert.assertAll();
    }

    @Test
    public void getSingleStudentTest() {
        int id = 0;
        Response response = RestAssured.get("http://localhost:5000/students/" + id);
        response.print();

        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200, "Status code is wrong");

        String fullname = response.jsonPath().getString("fullname");
        String registrationDate = response.jsonPath().getString("registrationDate");
        boolean isPaid = response.jsonPath().getBoolean("deposit.isPaid");
        List<String> classes = response.jsonPath().getList("classes");
        int actualId = response.jsonPath().getInt("id");

        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(fullname, "Jeremy Clarkson", "The fullname is wrong");
        softAssert.assertEquals(registrationDate, "2020-10-10", "The registrationDate is wrong");
        softAssert.assertTrue(isPaid, "isPaid is wrong");
        softAssert.assertEquals(classes.get(0), "manual", "First class is wrong");
        softAssert.assertEquals(actualId, id, "ID is wrong");

        softAssert.assertAll();
    }

}
