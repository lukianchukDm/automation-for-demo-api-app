package net.portnov;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class HealthcheckTests {

    @Test
    public void healthcheckTest() {
        Response response = RestAssured.get("https://portnov-students-demo-api.herokuapp.com/healthcheck");
        response.print();

        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200, "Status code is wrong");

        String message = response.jsonPath().getString("message");
        int uptime = response.jsonPath().getInt("uptime");

        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(message, "OK", "Expected message is wrong");
        softAssert.assertTrue(uptime > 0, "Expected uptime > 0, actual uptime: " + uptime);

        softAssert.assertAll();
    }
}
