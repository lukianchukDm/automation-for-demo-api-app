package net.portnov;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class ReplaceStudentTests {

    @Test
    public void replaceStudentTest() {
        int studentId = 0;

        JSONObject body = new JSONObject();
        body.put("fullname", "Matt LeBlanc");
        body.put("registrationDate", "2020-11-05");

        JSONObject deposit = new JSONObject();
        deposit.put("isPaid", true);
        deposit.put("amount", 750);
        body.put("deposit", deposit);

        body.put("group", "Campus");

        String[] classes = {"manual", "automation", "api"};
        body.put("classes", classes);

        Response response = RestAssured.given()
                .header(new Header("x-access-token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"))
                .contentType(ContentType.JSON)
                .body(body.toString())
                .put("https://portnov-students-demo-api.herokuapp.com/students/" + studentId);

        response.print();
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 201, "Status code is wrong");

        String fullname = response.jsonPath().getString("fullname");
        String registrationDate = response.jsonPath().getString("registrationDate");
        boolean isPaid = response.jsonPath().getBoolean("deposit.isPaid");
        List<String> newClasses = response.jsonPath().getList("classes");
        int newId = response.jsonPath().getInt("id");

        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(fullname, "Matt LeBlanc", "The fullname is wrong");
        softAssert.assertEquals(registrationDate, "2020-11-05", "The registrationDate is wrong");
        softAssert.assertTrue(isPaid, "isPaid is wrong");
        softAssert.assertEquals(classes, newClasses.toArray(), "Classes are wrong");
        softAssert.assertNotEquals(newId, studentId, "ID is wrong");

        softAssert.assertAll();
    }
}
